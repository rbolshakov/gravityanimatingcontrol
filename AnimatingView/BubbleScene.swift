//
//  BubbleScene.swift
//  AnimatingView
//
//  Created by roman on 26.12.2019.
//  Copyright © 2019 roman. All rights reserved.
//

import UIKit

let frequency: CGFloat = 2.0
let damping: CGFloat = 2.0
var commonScale: CGFloat = 1
let circleDistance: CGFloat = 2
let inactiveOpacity: Float = 0.5
let speakerZoom: CGFloat = 1.5

protocol BubbleSceneDelegate {
    func imageForUserId (userId: Int) -> UIImage?
}

internal class Figure {
    public private(set) var view: UIView!
    var imageView: UIImageView!
    
    var scale: CGFloat = 1
    var attachmentBehavior: UIAttachmentBehavior!
    var dynamicsBehavior: UIDynamicItemBehavior!
    var animationTimer: Timer?
    var circles: [UIView] = []
    
    var animating: Bool {
        return animationTimer != nil
    }
    
    var bounds: CGRect {
        get {
            return view.bounds
        }
        set {
            view.bounds = newValue
            view.layer.cornerRadius = newValue.size.width / 2
            imageView.layer.cornerRadius = newValue.size.width / 2 - circleDistance
        }
    }
    
    var frame: CGRect {
        get {
            return view.frame
        }
        set {
            view.frame = newValue
            view.layer.cornerRadius = newValue.size.width / 2
            imageView.layer.cornerRadius = newValue.size.width / 2 - circleDistance
        }
    }
    
    init() {
        view = UIView()
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 4
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let leadingConstraint = NSLayoutConstraint(item: imageView!, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: circleDistance)
        let trailingConstraint = NSLayoutConstraint(item: imageView!, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -circleDistance)
        let topConstraint = NSLayoutConstraint(item: imageView!, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: circleDistance)
        let bottomConstraint = NSLayoutConstraint(item: imageView!, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -circleDistance)
        view.addSubview(imageView)
        view.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
        
        //figure.view.backgroundColor = UIColor.init(red: CGFloat(Double.random(in: 0.0...1.0)), green: CGFloat(Double.random(in: 0.0...1.0)), blue: CGFloat(Double.random(in: 0.0...1.0)), alpha: 1) //Just for test purpose
        view.backgroundColor = .clear
        view.layer.opacity = inactiveOpacity
        view.layer.cornerRadius = view.bounds.size.height / 2
        imageView.layer.cornerRadius = view.layer.cornerRadius - circleDistance
    }
}

internal class User {
    var userId: Int!
    
    var figure: Figure! {
        didSet {
            figure.imageView.image = image
        }
    }
    
    weak var scene: BubbleScene!
    
    var active = false {
        willSet (_active) {
            if _active == false { speaking = false }
            figure.view.layer.opacity = _active ? 1 : inactiveOpacity
        }
    }
    
    var speaking = false {
        willSet(_speaking) {
            if _speaking == true { active = true }
            let zoom: CGFloat = _speaking ? speakerZoom : 1
            scene.zoomFigure(figure: figure, zoom: zoom)
            scene.setAnimationStateForFigure(figure, animating: _speaking)
        }
    }
    
    var delete = false {
        didSet {
            if delete {
                scene.removeFigure(figure)
            }
        }
    }
    
    var image: UIImage? {
        scene.delegate?.imageForUserId(userId: userId)
    }
    
    private init() {}
    
    init(_ userId: Int, scene: BubbleScene) {
        self.userId = userId
        self.scene = scene
        figure = scene.createFigure()
        figure.imageView.image = image
    }
}

protocol BubbleUsers {
    func setInactiveUsers(_ inactiveUsers: [Int])
    func setActiveUsers(_ activeUsers: [Int])
    func setSpeakingUsers(_ speakingUsers: [Int])
}

extension BubbleScene: BubbleUsers {
    func getUserById (_ userId: Int) -> User? {
        for user in users {
            if user.userId == userId {
                return user
            }
        }
        return nil
    }
    
    func setActiveUsers(_ activeUsers: [Int]) {
        for userId in activeUsers {
            if let currentUser = getUserById(userId) {
                currentUser.active = true
                currentUser.speaking = false
            } else {
                let user = User(userId, scene: self)
                user.userId = userId
                user.active = true
                users.append(user)
            }
        }
    }
    
    func removeUsers(with indexes: [Int]) {
        for index in indexes {
            let user = users[index]
            user.delete = true
        }
        
        users = users
        .enumerated()
        .filter { !indexes.contains($0.offset) }
        .map { $0.element }
    }
    
    func setInactiveUsers(_ inactiveUsers: [Int]) {
        var indexes: [Int] = []
        for (index, user) in users.enumerated() {
            if !user.active && !inactiveUsers.contains(user.userId) {
                indexes.append(index)
            }
        }
        
        removeUsers(with: indexes)
        
        for userId in inactiveUsers {
            if let currentUser = getUserById(userId) {
                currentUser.active = false
            } else {
                let user = User(userId, scene: self)
                user.userId = userId
                users.append(user)
            }
        }
    }
    
    func setSpeakingUsers(_ speakingUsers: [Int]) {
        for userId in speakingUsers {
            if let currentUser = getUserById(userId) {
                currentUser.speaking = true
            } else {
                let user = User(userId, scene: self)
                user.userId = userId
                user.speaking = true
                users.append(user)
            }
        }
    }
}

class BubbleScene: UIView {
    private var baseRadius: CGFloat!
    private var wavePlane: UIView!
    private var animator: UIDynamicAnimator!
    private var gravityBehavior: UIGravityBehavior!
    private var collisionsBehavior: UICollisionBehavior!
    var users: [User] = []
    var figures: [Figure] = []
//    private var figures:[Figure] { users
//        .map { $0.figure }
//    }
    var delegate: BubbleSceneDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupAnimator()
    }
    
    fileprivate func createFigure() -> Figure {
        let figure = Figure()
        figure.frame = initialFrameForFigure(figure)
        addSubview(figure.view)
        addBehaviorsForFigure(figure)
        figures.append(figure)
        checkSceneNeedsScale()
        return figure
    }
    
    fileprivate func removeFigure(_ figureToRemove: Figure) {
        figureToRemove.view.removeFromSuperview()
        var removeIndex: Int? = nil
        for (index, figure) in figures.enumerated() {
            if figureToRemove.view == figure.view {
                removeIndex = index
                break
            }
        }
        if let removeIndex = removeIndex {
            figures.remove(at: removeIndex)
        }
        checkSceneNeedsScale()
    }
    
    private func initialFrameForFigure(_ figure: Figure) -> CGRect {
        
        enum SceneCorner: Int {
            case topLeft = 1, topRight, bottomLeft, bottomRight

            static func random() -> SceneCorner {
                let rand = Int.random(in: 1...4)
                return SceneCorner(rawValue: rand)!
            }
        }
        
        let dimension = 2 * baseRadius * commonScale * figure.scale
        let randomizeX = CGFloat.random(in: 1...10)
        let randomizeY = CGFloat.random(in: 1...10)
        let corner = SceneCorner.random()
        var frame: CGRect!
        switch corner {
        case .topLeft:
            frame = CGRect(x: 0 + randomizeX, y: 0 + randomizeY, width: dimension, height: dimension)
        case .topRight:
            frame = CGRect(x: self.frame.size.width - dimension - randomizeX, y: 0 + randomizeY, width: dimension, height: dimension)
        case .bottomLeft:
            frame = CGRect(x: 0 + randomizeX, y: self.frame.size.height - dimension - randomizeY, width: dimension, height: dimension)
        case .bottomRight:
            frame = CGRect(x: self.frame.size.width - dimension - randomizeX, y: self.frame.size.height - dimension - randomizeY, width: dimension, height: dimension)
        }
        
        return frame
    }
    
    private func addBehaviorsForFigure(_ figure: Figure) {
        collisionsBehavior.addItem(figure.view)
        
        figure.attachmentBehavior = UIAttachmentBehavior(item: figure.view, attachedToAnchor: CGPoint(x:self.bounds.size.width/2, y: self.bounds.size.height/2))
        figure.attachmentBehavior.length = 0
        figure.attachmentBehavior.damping = damping
        figure.attachmentBehavior.frequency = frequency
        animator.addBehavior(figure.attachmentBehavior)
        
        figure.dynamicsBehavior = UIDynamicItemBehavior(items: [figure.view])
        figure.dynamicsBehavior.allowsRotation = false
        animator.addBehavior(figure.dynamicsBehavior)
        
        /*
        TOO DIFFICULT FEATURE TO USE
        attachmentBehavior.action = {
            if self.needsScale {
                figure.transform = CGAffineTransform.init(scaleX: 2, y: 2)
                self.animator.updateItem(usingCurrentState: figure)
            }
        }*/
    }
    
    private func setupAnimator() {
        wavePlane = UIView()
        wavePlane.frame = self.bounds
        addSubview(wavePlane)
        
        baseRadius = self.bounds.width / 5  //Just initial value, doesn't seem to matter much
        
        animator = UIDynamicAnimator(referenceView: self)
        
        gravityBehavior = UIGravityBehavior()
        gravityBehavior.gravityDirection = CGVector(dx: 0, dy: 0)
        animator.addBehavior(gravityBehavior)
        
        collisionsBehavior = UICollisionBehavior()
        collisionsBehavior.translatesReferenceBoundsIntoBoundary = true
        animator.addBehavior(collisionsBehavior)
    }
    
    private func checkSceneNeedsScale() {
        let screenSpace = self.bounds.width * self.bounds.height / CGFloat.pi //using only quarter of scene space
        var figuresSpace: CGFloat = 0
        for figure in figures {
            var space = baseRadius * baseRadius * figure.scale * figure.scale * 3.14
            space *= commonScale
            space *= commonScale
            figuresSpace += space
        }

        let factor = (screenSpace / figuresSpace).squareRoot()
        
        print ("screen space= \(screenSpace)")
        print ("total figures = \(figures.count), scale = \(commonScale), figures' space = \(figuresSpace)")
        print ("factor = \(factor)")

        scaleAllFigures(factor: factor)
    }
    
    fileprivate func zoomFigure(figure: Figure, zoom: CGFloat) {
        
          animator.removeBehavior(figure.attachmentBehavior)
          animator.removeBehavior(figure.dynamicsBehavior)
          animator.removeBehavior(gravityBehavior)
          animator.removeBehavior(collisionsBehavior)

          figure.scale = zoom
          let dimension = 2 * baseRadius * commonScale * figure.scale
        
          figure.bounds = CGRect(x: 0, y: 0, width: dimension, height: dimension)
    
          animator.addBehavior(figure.attachmentBehavior)
          animator.addBehavior(figure.dynamicsBehavior)
          animator.addBehavior(gravityBehavior)
          animator.addBehavior(collisionsBehavior)
          
          checkSceneNeedsScale()
    }
    
    private func scaleAllFigures(factor: CGFloat) {
        commonScale *= factor
        animator.removeBehavior(gravityBehavior)
        animator.removeBehavior(collisionsBehavior)
        for figure in figures {
            animator.removeBehavior(figure.attachmentBehavior)
            animator.removeBehavior(figure.dynamicsBehavior)
            let dimension = 2 * baseRadius * commonScale * figure.scale
            
            figure.bounds = CGRect(x: 0, y: 0, width: dimension, height: dimension)
            
            animator.addBehavior(figure.attachmentBehavior)
            animator.addBehavior(figure.dynamicsBehavior)
        }
        animator.addBehavior(gravityBehavior)
        animator.addBehavior(collisionsBehavior)
    }

    //MARK: animations
    fileprivate func setAnimationStateForFigure(_ figure: Figure, animating: Bool) {
        if animating {
            guard !figure.animating else { return; }
            //Add two close circle waves
            figure.animationTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] timer in
                self?.addAnimatingCircleForFigure(figure)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self?.addAnimatingCircleForFigure(figure)
                })
            }
        } else {
            figure.animationTimer?.invalidate()
            figure.animationTimer = nil
            for circle in figure.circles {
                circle.removeFromSuperview()
            }
        }
    }
    
    private func addAnimatingCircleForFigure(_ figure: Figure) {
        let circle = createWaveCircle(figure.view.center)
        figure.circles.append(circle)
        wavePlane.addSubview(circle)
        animateWaveCircle(circle)
    }
    
    private func createWaveCircle(_ center: CGPoint) -> UIView {
        let circle = UIView()
        circle.frame = CGRect(x: center.x - baseRadius * commonScale, y: center.y - baseRadius * commonScale, width: 2 * baseRadius * commonScale, height: 2 * baseRadius * commonScale)
        circle.layer.cornerRadius = baseRadius * commonScale
        circle.backgroundColor = .white
        circle.layer.opacity = 0.5
        return circle
    }
    
    private func animateWaveCircle(_ circle: UIView) {
        UIView.animate(withDuration: 1.5, animations: {
            circle.transform = CGAffineTransform.init(scaleX: 20, y: 20)
        }, completion: { completed in
            circle.removeFromSuperview()
        })
        
        UIView.animateKeyframes(withDuration: 1.5, delay: 0, options: .calculationModeCubic, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.05) {
                circle.layer.opacity = 0.3
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.05, relativeDuration: 0.05) {
                circle.layer.opacity = 0.2
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.15) {
                circle.layer.opacity = 0.1
            }

            UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 1.25) {
                circle.layer.opacity = 0
            }
        }) { _ in }
    }
}
