//
//  ViewController.swift
//  AnimatingView
//
//  Created by roman on 25.12.2019.
//  Copyright © 2019 roman. All rights reserved.
//

import UIKit
class ViewController: UIViewController, BubbleSceneDelegate {
    var totalUsers: [Int] = [1,2,3,4,5,6,7,8,9,10];
    var inactiveUsers: [Int] = [];
    var activeUsers: [Int] = [];
    var speakingUsers: [Int] = [];
    func imageForUserId(userId: Int) -> UIImage? {
        return UIImage(named: "\(userId)")
    }
    
    @IBOutlet  var bubbleScene: BubbleScene!
    override func viewDidLoad() {
        super.viewDidLoad()
        bubbleScene.delegate = self
    }
    
    @IBAction func addInactiveUser() {
        guard totalUsers.count > 0 else { return; }
        let userIndex = Int.random(in: 0..<totalUsers.count)
        inactiveUsers.append(totalUsers[userIndex])
        totalUsers.remove(at: userIndex)
        bubbleScene.setInactiveUsers(inactiveUsers)
    }
    
    @IBAction func addActiveUser() {
        guard inactiveUsers.count > 0 else { return; }
        let userIndex = Int.random(in: 0..<inactiveUsers.count)
        activeUsers.append(inactiveUsers[userIndex])
        inactiveUsers.remove(at: userIndex)
        bubbleScene.setActiveUsers(activeUsers)
    }
    
    @IBAction func addSpeakingUser() {
        guard activeUsers.count > 0 else { return; }
        let userIndex = Int.random(in: 0..<activeUsers.count)
        speakingUsers.append(activeUsers[userIndex])
        activeUsers.remove(at: userIndex)
        bubbleScene.setSpeakingUsers(speakingUsers)
    }
}

